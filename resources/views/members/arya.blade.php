<!DOCTYPE html>
<html>
<head> 
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <style>
        #slika {
            width: 800px;                
        }
        
    </style>   
</head>

<body>

  <!-- Page Content -->
  <div class="container">

    <div class="row">
    
      <!-- Post Content Column -->
      <div class="col-8">
        
        <!-- Title -->
        <h1 class="mt-4">Arya Stark</h1>

        <!-- Preview Image -->
        <img id='slika' src="https://cdn.vox-cdn.com/thumbor/f1fPI105nOXsl_XTbbTFD4XKrdE=/0x0:2700x3992/1200x675/filters:focal(1161x1656:1593x2088)/cdn.vox-cdn.com/uploads/chorus_image/image/63747029/77ea957c0eb6306d843b32c4dfd16b71.0.jpg" alt="" >

        <!-- Post Content -->
        <br>
        <p>Princess Arya Stark is the third child and second daughter of Lord Eddard Stark and his wife, Lady Catelyn Stark. She is the sister of the incumbent Westerosi monarchs, Sansa, Queen in the North, and Brandon, King of the Andals and the First Men.</p>

        <p>After narrowly escaping the persecution of House Stark by House Lannister, Arya is trained as a Faceless Man at the House of Black and White in Braavos, using her abilities to avenge her family. Upon her return to Westeros, she exacts retribution for the Red Wedding by exterminating the Frey male line.</p>

        <p>Returning to Winterfell, Arya initially finds herself at odds with Sansa due to the naïvety of her youth and supposed lack of support for their "half-brother", Jon Snow, King in the North. However, their tumultuous relationship is discovered to be the result of Petyr Baelish's manipulation, and the two mend their sisterhood following his execution for his crimes against House Stark.</p>

        <p>Arya then plays a significant role in the Great War, effectively ending the conflict through her killing of the Night King during the Battle of Winterfell. Afterwards, however, she rides south with Sandor Clegane, not intent on returning to Winterfell, arriving at King's Landing as it is being destroyed by Daenerys Targaryen. Sandor convinces Arya to abandon her quest for vengeance, leading Arya to instead try, though fail, to save the smallfolk. Following Jon's exile to the Wall for regicide, as punishment for murdering Daenerys, and the ascensions of her siblings, Arya decides to leave Westeros and sail west to discover what lies beyond where the maps of the known world end.</p>
        
        <blockquote class="blockquote">
          <p class="mb-0"><i> Nothing isn't better or worse than anything. Nothing is just nothing. </i></p>
          <footer class="blockquote-footer">Arya and a dying man.
            <cite title="Source Title"></cite>
          </footer>
        </blockquote>

        <h3>Personality</h3>

        <p>Arya is a fiercely independent woman who is unconstrained by social expectations like gender roles, courtly virtues, class distinctions, and the expectations of her parents and siblings. A tomboy, she never aspired to be a "proper lady" as her older sister Sansa did. Before the series of events that shatter her innocence and destroy her support system, Arya is full of life, and she makes others smile just by virtue of her spirited indifference to rules. </p>

        <p>Arya can be a rather cold-blooded and slightly sadistic person at times, especially while confronting and killing those who are on her death list. Though she was initially apathetic but satisfied with killing Meryn Trant and Polliver, she did not attempt to hide her glee over slitting Walder Frey's throat, or the pleasure of seeing his family dying from poisoned wine she gave them. While she certainly possesses compassion and kindness, her time with The Hound and the Faceless Men have taught her to be ruthless to those who have wronged her and her family and has shown to be willing to use psychological mind games to worsen her enemies fate before killing them. This is shown when she repeated every word Polliver said to Lommy before killing him in the same exact way with Needle; gouged the eyes of Ser Meryn before stabbing him repeatedly and butchering Lord Walder's sons before serving their corpses to their father inside of a pie</p>

        <p>Arya seems to prefer staying out of politics. During a war meeting, planning for the Battle of King's Landing, Arya was clearly distrustful of Daenerys when she wanted northern troops to fight immediately. However, she did not actively speak out against her, preferring to let Sansa speak on her behalf. Later, instead of taking any action involving the truth about Jon's true parentage, she instead rides with the Hound to King's Landing to kill Cersei. Later, during the council meeting to elect the next monarch Arya withholds her vote for Bran since Sansa declares the North an independent kingdom and only speaks to threaten Yara when she suggests letting the unsullied keep Jon. All of this speaks to her preferring to let Sansa speak for her family on political matters and for her to focus on confronting threats to her family, friends, and herself in a more direct matter, outside of politics.</p>
    
        <a href="/" class="btn btn-outline-dark">Go back</a>
        
      </div>
      
      <div class='col-1'></div>
      
      <div class="col-3">      
        <!-- Search Widget -->
        <div class="card my-4">
          <h5 class="card-header">Search</h5>
          <div class="card-body">
            <div class="input-group">
              <input class="form-control" placeholder="Search for..." type="text">
              <span class="input-group-btn">
                <button class="btn btn-secondary" type="button">Go!</button>
              </span>
            </div>
          </div>
        </div>
    </div> 
    </div>
    <!-- /.row -->

  </div>
  <!-- /.container -->
    <br>
  <!-- Footer -->
  <footer class="py-5 bg-dark">
    <div class="container">
      <p class="m-0 text-center text-white">Copyright © The Starks 2019</p>
    </div>
    <!-- /.container -->
  </footer>

  <!-- Bootstrap core JavaScript -->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

</body>
</html>