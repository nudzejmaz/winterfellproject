<!DOCTYPE html>
<html>
<head> 
<link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <style>
        #id {
            float: left;
            margin: 20px;
            width: 700px;
        }

        p {
            margin: 20px;
        }

        body {
        }
    </style>
</head>
<body>

  <!-- Page Content -->
  <div class="container">

    <div class="row">

      <!-- Post Content Column -->
      <div class="col-lg-8">

        <!-- Title -->
        <br>
        <h1 class="naslov">Sansa Stark</h1>

        <!-- Preview Image -->
        <img id ="id" src="https://media1.popsugar-assets.com/files/thumbor/GGxZZVNyZlY1qhOerZjp6sjupSQ/fit-in/1200x630/filters:format_auto-!!-:strip_icc-!!-:fill-!white!-/2019/02/28/971/n/1922283/ee29e9875c785e6808a6d4.83849433_.jpg" alt="" float: left>
        
        <br>
        <!-- Post Content -->
        
        <br>
        
        <p>


Queen Sansa Stark is the eldest daughter of Lord Eddard Stark and his wife, Lady Catelyn, sister of Robb, Arya, Bran, and Rickon Stark, and "half-sister" of Jon Snow.

Initially betrothed to Prince Joffrey Baratheon, the heir to the Iron Throne, Sansa becomes well versed in politics and court intrigue following the execution of her father, under the tutelage of Cersei Lannister, Margaery Tyrell and Petyr Baelish, suffering but learning from her traumatic experiences as a hostage of House Lannister in King's Landing and House Bolton at Winterfell. </p>
<p>After escaping from the latter, with the help of her father's ward, Theon Greyjoy, and her sworn-sword, Brienne of Tarth, she reunites with Jon at Castle Black. Alongside him, Sansa retakes Winterfell from Ramsay Bolton, becoming the Lady of Winterfell, whilst Jon is declared King in the North.

Displeased at Jon abdicating in favour of House Targaryen, whose forces prove vital during the Great War, she finds enmity with Queen Daenerys, who refuses to accept Northern independence in her restoration. Following the Battle of King's Landing and the subsequent assassination of Daenerys, Sansa declares the North an independent kingdom and is later crowned Queen in the North. </p>

        <blockquote class="blockquote">
          <p class="mb-0"><i>"I did what I had to do to survive, my lady. But I am a Stark, I will always be a Stark."</i> </p>
          <footer class="blockquote-footer"><i>Sansa to Lyanna Mormont</i>
            <cite title="Source Title"></cite>
          </footer>
        </blockquote>

        <p> When she lived at Winterfell with her family, Sansa grew up as the eldest daughter of a Great House, trying to emulate her mother's example of a "proper lady" from the southern courts. Sansa's devotion to the traditional, refined "feminine virtues" caused friction between her and her blunt, tomboyish younger sister Arya, with whom she had a sibling rivalry. As a little girl, Sansa naively believed in the tales and epic romances in which every princess gets her honorable knight in shining armor to sweep her off her feet. Sansa was infatuated with the traditional romances about mythical figures like Jonquil, and historical figures like Duncan Targaryen. Her greatest goal in life was to be married to a heroic and handsome prince, sitting around with other noblewomen eating lemon cakes while gossiping about the goings-on at court.</p>

<p>Sansa's innocent, childhood infatuation with the ideals of princesses and knights made her tragically susceptible to the manipulations of the Lannisters. She held the blind belief that all queens and princes are kind and truthful, as if inherently a result of their titles, when Cersei just happened to be a woman who married a king to secure a marriage alliance, and Joffrey just happened to be her son. Without real cause, she blindly loved Joffrey with all her heart and trusted and admired Cersei, only for them to repay her love and trust by beheading her father before her very eyes.

Afterwards, Sansa has a harrowing experience as a prisoner of the Lannisters, a plaything for the psychopathic Joffrey to have publicly beaten by his guards for petty amusement. Her shock at the death of her father was only later deepened at the news of how her mother and brother Robb were horrifically killed at the Red Wedding and their corpses desecrated. She was visibly elated upon hearing that Arya, Bran, and Rickon were actually still alive, and though she took mostly after her mother and had a somewhat distant relationship with Jon, she loved him all the same. She was surprised to hear that Jon had become Lord Commander of the Night's Watch, and Jon was the first person she turns to for shelter after escaping Winterfell, aware that he would protect her from Ramsay.

Sansa subsequently escapes King's Landing to the Vale with Littlefinger, and her experiences have clearly changed her personality. She had to learn from painful experience how to lie to survive at Joffrey's court, saying one thing but meaning another and manipulating people to her own ends as best she could. No longer under constant control by Joffrey and Cersei's guards, she is in her own way beginning to mature into her own power and influence as a political force.</p>

<p>The numerous tragedies she has suffered, and the crimes against herself and her family, have also darkened her personality turning her more ruthless, though not quite cruel. This shows when she refuses to take Theon's arm when he comes to escort her to the Godswood to marry Ramsay and coldly asks if he thinks she cares what Ramsay will do to him if she doesn't. She also later expresses approval of what Ramsay did to Theon, now Reek, and declares she would do the same. Once reclaiming Winterfell, Sansa feeds her abusive husband, Ramsay to his own hounds, even smiling as it happened. </p>

<a href="/" class="btn btn-outline-dark">Go back</a>
    </div>
<div class='col-1'></div>
      <div class="col-3">      
        <!-- Search Widget -->
        <div class="card my-4">
          <h5 class="card-header">Search</h5>
          <div class="card-body">
            <div class="input-group">
              <input class="form-control" placeholder="Search for..." type="text">
              <span class="input-group-btn">
                <button class="btn btn-secondary" type="button">Go!</button>
              </span>
            </div>
          </div>
        </div>
    </div>
    </div>
    <!-- /.row -->

  </div>
  <!-- /.container -->
<br>
  <!-- Footer -->
  <footer class="py-5 bg-dark">
    <div class="container">
      <p class="m-0 text-center text-white">Copyright © The Starks 2019</p>
    </div>
    <!-- /.container -->
  </footer>

  <!-- Bootstrap core JavaScript -->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

</body>
</html>