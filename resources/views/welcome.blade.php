<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            @if (Route::has('login'))
                <div class="top-right links">
                    @auth
                        <a href="{{ url('/home') }}">Home</a>
                    @else
                        <a href="{{ route('login') }}">Login</a>

                        @if (Route::has('register'))
                            <a href="{{ route('register') }}">Register</a>
                        @endif
                    @endauth
                </div>
            @endif

            <div class="content">
                <div class="title m-b-md">
                    Laravel
                </div>

                <section class="dentist-area section-padding-100-0">
    <div class="container">
      <div class="row">
        <!-- Section Heading -->
        <div class="col-12">
          <div class="section-heading text-center">
            <h2>Our Dentist</h2>
            <div class="line"></div>
          </div>
        </div>
      </div>

      <div class="row">
        <!-- Single Dentist Area -->
        <div class="col-12 col-sm-6 col-md-4">
          <div class="single-dentist-area mb-100">
            <img src="./img/bg-img/9.png" alt="">
            <!-- Dentist Content -->
            <div class="dentist-content">
              <!-- Social Info -->
              <div class="dentist-social-info">
                <a href="#"><i class="fa fa-facebook"></i></a>
                <a href="#"><i class="fa fa-twitter"></i></a>
                <a href="#"><i class="fa fa-google-plus"></i></a>
              </div>
              <!-- Dentist Info -->
              <div class="dentist-info bg-gradient-overlay">
                <h5>Michael Barley</h5>
                <p>Implant Expert</p>
              </div>
            </div>
          </div>
        </div>

        <!-- Single Dentist Area -->
        <div class="col-12 col-sm-6 col-md-4">
          <div class="single-dentist-area mb-100">
            <img src="./img/bg-img/10.png" alt="">
            <!-- Dentist Content -->
            <div class="dentist-content">
              <!-- Social Info -->
              <div class="dentist-social-info">
                <a href="#"><i class="fa fa-facebook"></i></a>
                <a href="#"><i class="fa fa-twitter"></i></a>
                <a href="#"><i class="fa fa-google-plus"></i></a>
              </div>
              <!-- Dentist Info -->
              <div class="dentist-info bg-gradient-overlay">
                <h5>Michael Barley</h5>
                <p>Implant Expert</p>
              </div>
            </div>
          </div>
        </div>

        <!-- Single Dentist Area -->
        <div class="col-12 col-sm-6 col-md-4">
          <div class="single-dentist-area mb-100">
            <img src="./img/bg-img/11.png" alt="">
            <!-- Dentist Content -->
            <div class="dentist-content">
              <!-- Social Info -->
              <div class="dentist-social-info">
                <a href="#"><i class="fa fa-facebook"></i></a>
                <a href="#"><i class="fa fa-twitter"></i></a>
                <a href="#"><i class="fa fa-google-plus"></i></a>
              </div>
              <!-- Dentist Info -->
              <div class="dentist-info bg-gradient-overlay">
                <h5>Michael Barley</h5>
                <p>Implant Expert</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

                <div class="links">
                    <a href="https://laravel.com/docs">Docs</a>
                    <a href="https://laracasts.com">Laracasts</a>
                    <a href="https://laravel-news.com">News</a>
                    <a href="https://blog.laravel.com">Blog</a>
                    <a href="https://nova.laravel.com">Nova</a>
                    <a href="https://forge.laravel.com">Forge</a>
                    <a href="https://github.com/laravel/laravel">GitHub</a>
                </div>
            </div>
        </div>
    </body>
</html>
