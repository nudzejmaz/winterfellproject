<!DOCTYPE html>
<html>
<head>
<link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <style>
        #id {
            float: left; 
            margin: 20px;  
            width: 700px;        
        }
        p {
            margin: 20px;
           
        }
    </style>

</head>
<body>

  <!-- Page Content -->
  <div class="container">

    <div class="row">

      <!-- Post Content Column -->
      <div class="col-8">

        <!-- Title -->
        <h1 class="mt-4">Bran Stark</h1>

        <!-- Preview Image -->
        <img id="id" src="https://cdn.inquisitr.com/wp-content/uploads/2019/05/Brandon-Stark.jpg" alt="">

        <br>

        <p>King Bran I the Broken, born Brandon Stark and commonly known simply as "Bran", is the fourth child and second son of Eddard and Catelyn Stark. Bran is a warg and a greenseer serving as the new Three-Eyed Raven, using his supernatural gifts in the war against the Night King and the White Walkers in which the living ultimately emerge victorious. After the assassination of Daenerys Targaryen at the hands of Jon Snow, Bran was chosen to ascend to the throne by a gathering of the remaining great Westerosi lords and ladies.

As the twenty-second ruler (and the first elective monarch) of the Six Kingdoms (so named following the secession of the North), he is styled as Bran the Broken, the First of His Name, King of the Andals and the First Men, Lord of the Six Kingdoms, and Protector of the Realm, the first king to drop his family name from his formal name and titles. </p>

        <p>Bran is the fourth child and second son of Lady Catelyn and Lord Ned Stark. Ned is the head of House Stark, Lord Paramount of the North, and Warden of the North to King Robert Baratheon. The North is one of the constituent regions of the Seven Kingdoms and House Stark is one of the Great Houses of the realm. House Stark rules the region from their seat of Winterfell.[2]

Bran was born and raised at Winterfell. He has an older brother Robb, a younger brother Rickon, two older sisters Sansa and Arya, and an older "bastard half-brother", Jon Snow, who is actually Bran's first cousin by his aunt Lyanna Stark. Bran was named for Ned's elder brother, Brandon, who was brutally executed by the Mad King along with Bran's paternal grandfather Rickard Stark. He is only called "Brandon" by his mother when he has done something wrong. Bran dreams of being a knight of the Kingsguard, and his favorite hobby is climbing the walls of Winterfell, using its old rooftops and passageways to get around.</p>


        <p>The very first—and youngest—point of view character in the novels, Bran was set up by Martin as a young hero of the series. Mikal Gilmore of Rolling Stone noted in 2014 that the moment in A Game of Thrones in which Jaime Lannister pushes Bran to his likely death "grabs you by the throat". Martin commented in the interview:

I've had a million people tell me that was the moment that hooked them, where they said, "Well, this is just not the same story I read a million times before." Bran is the first viewpoint character. In the back of their heads, people are thinking Bran is the hero of the story. He's young King Arthur. We're going to follow this young boy—and then, boom: You don't expect something like that to happen to him. So that was successful [laughs].[1] 

In 2000, Martin called Bran the hardest character to write:

Number one, he is the youngest of the major viewpoint characters, and kids are difficult to write about. I think the younger they are, the more difficult. Also, he is the character most deeply involved in magic, and the handling of magic and sorcery and the whole supernatural aspect of the books is something I'm trying to be very careful with. So I have to watch that fairly sharply. All of which makes Bran's chapters tricky to write.[3] 

Booklist cited Bran as a notable character in 1999, and the Publishers Weekly review of A Game of Thrones noted, "It is fascinating to watch Martin's characters mature and grow, particularly Stark's children, who stand at the center of the book."

Noting Bran's absence in 2005's A Feast for Crows, James Poniewozik of Time wrote in his review of A Dance with Dragons (2011):

Some favorite characters were MIA for eleven long years. ADWD brings them back—bastard warrior Jon Snow, exiled dragon queen Daenerys Targaryen, fugitive dwarf Tyrion Lannister and crippled, mystical Bran Stark, among others—and almost from the get-go that gives it a narrative edge over its companion book. Each, in his or her own way, is dealing with a question of power. </p>

<blockquote class="blockquote">
        <cite title="Source Title">"The things we do for love."</cite>
          <footer class="blockquote-footer">Bran Stark in
            <cite title="Source Title">Season 8</cite>
          </footer>
        </blockquote>


        <p>The very first—and youngest—point of view character in the novels, Bran was set up by Martin as a young hero of the series. Mikal Gilmore of Rolling Stone noted in 2014 that the moment in A Game of Thrones in which Jaime Lannister pushes Bran to his likely death "grabs you by the throat". Martin commented in the interview:

I've had a million people tell me that was the moment that hooked them, where they said, "Well, this is just not the same story I read a million times before." Bran is the first viewpoint character. In the back of their heads, people are thinking Bran is the hero of the story. He's young King Arthur. We're going to follow this young boy—and then, boom: You don't expect something like that to happen to him. So that was successful [laughs].[1] 

In 2000, Martin called Bran the hardest character to write:

Number one, he is the youngest of the major viewpoint characters, and kids are difficult to write about. I think the younger they are, the more difficult. Also, he is the character most deeply involved in magic, and the handling of magic and sorcery and the whole supernatural aspect of the books is something I'm trying to be very careful with. So I have to watch that fairly sharply. All of which makes Bran's chapters tricky to write.[3] 

Booklist cited Bran as a notable character in 1999, and the Publishers Weekly review of A Game of Thrones noted, "It is fascinating to watch Martin's characters mature and grow, particularly Stark's children, who stand at the center of the book."

Noting Bran's absence in 2005's A Feast for Crows, James Poniewozik of Time wrote in his review of A Dance with Dragons (2011):

Some favorite characters were MIA for eleven long years. ADWD brings them back—bastard warrior Jon Snow, exiled dragon queen Daenerys Targaryen, fugitive dwarf Tyrion Lannister and crippled, mystical Bran Stark, among others—and almost from the get-go that gives it a narrative edge over its companion book. Each, in his or her own way, is dealing with a question of power. </p>

<br>

<a href="/" class="btn btn-outline-dark">Go back</a>

         </div>
      <div class='col-1'></div>
      <div class="col-3">  
      <div class="card my-4">
          <h5 class="card-header">Search</h5>
          <div class="card-body">
            <div class="input-group">
              <input class="form-control" placeholder="Search for..." type="text">
              <span class="input-group-btn">
                <button class="btn btn-secondary" type="button">Go!</button>
              </span>
            </div>
          </div>
        </div>
    </div>

    </div>
    <!-- /.row -->

  </div>
  <!-- /.container -->
<br>
  <!-- Footer -->
  <footer class="py-5 bg-dark">
    <div class="container">
      <p class="m-0 text-center text-white">Copyright © The Starks 2019</p>
    </div>
    <!-- /.container -->
  </footer>

  <!-- Bootstrap core JavaScript -->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

</body>
</html>

