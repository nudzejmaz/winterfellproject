@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="card-columns">
                {{-- <div class="card-header">Dashboard</div> --}}
                <!--@for($i = 0; $i < 1; $i++) -->
                <div class="card" style="width: 20rem;">
 
                    <img class="card-img-top" src="https://vignette.wikia.nocookie.net/gameofthrones/images/6/63/QueenSansa.PNG/revision/latest?cb=20190520085809" height="475px" alt="the image alt text here">
                   
                    <div class="card-body">
                   
                      <h5 class="card-title text-primary"><b>Sansa Stark</b></h5>
                   
                      <p class="card-text">Queen <i> Sansa Stark </i>is the eldest daughter of Lord <i>Eddard Stark</i> and his wife, <i>Lady Catelyn</i>, sister of <i>Robb</i>, <i>Arya</i>, <i>Bran</i>, and <i>Rickon Stark</i>, and "half-sister" of <i>Jon Snow</i>.
                   
                      </p>
                   
                      <a href="/showSansa" class="btn btn-outline-dark">Learn more</a>
                   
                    </div>
                   
                  </div>
               <!-- @endfor-->
               <div class="card" style="width: 20rem;">
 
                    <img class="card-img-top" src="https://vignette.wikia.nocookie.net/gameofthrones/images/b/be/AryaShipIronThrone.PNG/revision/latest?cb=20190520174300" alt="the image alt text here">
                   
                    <div class="card-body">
                   
                      <h5 class="card-title text-primary"><b>Arya Stark</b></h5>
                   
                      <p class="card-text">Princess <i>Arya Stark</i> is the third child and second daughter of <i>Lord Eddard Stark</i> and his wife, <i>Lady Catelyn Stark</i>. She is the sister of the incumbent Westerosi monarchs, <i>Sansa</i>, Queen in the North, and <i>Brandon</i>, King of the Andals and the First Men.
                      </p>
                   
                      <a href="/showArya" class="btn btn-outline-dark">Learn more</a>
                   
                    </div>
                   
                  </div>

                  <div class="card" style="width: 20rem;">
 
                    <img class="card-img-top" src="https://vignette.wikia.nocookie.net/gameofthrones/images/8/81/KingBran.PNG/revision/latest?cb=20190520173855" alt="the image alt text here">
                   
                    <div class="card-body">
                   
                      <h5 class="card-title text-primary"><b>Brandon Stark</b></h5>
                   
                      <p class="card-text">King Bran I the Broken, born <i>Brandon Stark</i> and commonly known simply as "Bran", is the fourth child and second son of <i>Eddard</i> and <i>Catelyn Stark</i>.
                   
                      </p>
                   
                      <a href="/showBran" class="btn btn-outline-dark">Learn more</a>
                   
                    </div>
                   
                  </div>
        </div>
    </div>
</div>
@endsection
