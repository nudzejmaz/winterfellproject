<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/showSansa', 'PostController@showSansa')->name('posts.showSansa');
Route::get('/showArya', 'PostController@showArya')->name('posts.showArya');
Route::get('/showBran', 'PostController@showBran')->name('posts.showBran');


Route::prefix('posts')->group(function() {
    Route::get('/', 'PostController@index')->name('posts.index');
   // Route::get('/show', 'PostController@show')->name('posts.show');
    Route::get('/create', 'PostController@create')->middleware('auth')->name('posts.create');
    Route::post('/store', 'PostController@store')->middleware('auth')->name('posts.store');
    Route::get('/{post}/edit', 'PostController@edit')->middleware('auth')->name('posts.edit');
    Route::patch('/{post}/update', 'PostController@update')->middleware('auth')->name('posts.update');
    Route::delete('/{post}/delete', 'PostController@destroy')->middleware('auth')->name('posts.destroy');
});

