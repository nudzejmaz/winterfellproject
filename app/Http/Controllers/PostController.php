<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PostController extends Controller
{
    public function showSansa()
    {
        return view('members.sansa');
    }

    public function showArya()
    {
        return view('members.arya');
    }

    public function showBran()
    {
        return view('bran');
    }

}
